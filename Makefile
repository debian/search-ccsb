SCRIPT=/usr/local/bin/
LISP=/usr/share/emacs21/site-lisp/


all: search-ccsb.elc

search-ccsb.elc: search-ccsb.el
	emacs -batch -q -f batch-byte-compile search-ccsb.el 


install: search-ccsb.elc
	sh ./install

clean:
	rm -f search-ccsb.elc *~

man:
	@pod2man --release="search-ccsb" --date="`date +"%B %e, %Y"`" --center="BibTeX search tool" search-ccsb > search-ccsb.1

.PHONY: install

